import React, {PureComponent} from "react";
import PropTypes from "prop-types";

import {GameType} from "../../const.js";


const withUserAnswer = (Component) => {
  class WithUserAnswer extends PureComponent {
    constructor(props) {
      super(props);

      const {question} = this.props;
      const {answers} = question;

      this.state = {
        answers: new Array(answers.length).fill(false),
      };

      this._answerHandle = this._answerHandle.bind(this);
      this._changeHandle = this._changeHandle.bind(this);
    }

    render() {
      const {answers} = this.state;

      return (
        <Component
          {...this.props}
          userAnswers={answers}
          onAnswer={this._answerHandle}
          onChange={this._changeHandle}
        />
      );
    }

    _answerHandle() {
      const {onAnswer, question} = this.props;
      const {answers} = this.state;

      onAnswer(question, answers);
    }

    _changeHandle(i, value) {
      const {answers} = this.state;

      const userAnswers = answers.slice();
      userAnswers[i] = value;

      this.setState({
        answers: userAnswers,
      });
    }
  }

  WithUserAnswer.propTypes = {
    question: PropTypes.shape({
      answers: PropTypes.arrayOf(PropTypes.shape({
        src: PropTypes.string.isRequired,
        genre: PropTypes.string.isRequired,
      })).isRequired,
      genre: PropTypes.string.isRequired,
      type: PropTypes.oneOf([GameType.ARTIST, GameType.GENRE]).isRequired,
    }).isRequired,
    onAnswer: PropTypes.func.isRequired,
  };

  return WithUserAnswer;
};


export default withUserAnswer;
