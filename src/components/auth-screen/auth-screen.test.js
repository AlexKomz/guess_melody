import React from "react";
import renderer from "react-test-renderer";
import {Router} from "react-router";

import history from "../../history.js";

import AuthScreen from "./auth-screen.jsx";


const noop = () => {};

it(`AuthScreen component render correctly`, () => {
  const tree = renderer.create(
      <Router
        history={history}
      >
        <AuthScreen
          onReplayButtonClick={noop}
          onSubmit={noop}
        />
      </Router>
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
